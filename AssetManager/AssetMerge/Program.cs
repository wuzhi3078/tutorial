﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using AssetLibrary;
namespace AssetMerge
{	
	internal static class Config {
	}

	class MainClass
	{
		[Conditional ("DEBUG")]
		static void InitCommandLineArgs (ref string[] args) {
			args = new string[] {
				"-s", "", 
				"-d", "",
				"-mf", @"^[.]"
			};
		}

		public static void Main (string[] args)
		{
			if (args.Length == 1 && args [0] == "--help") {
				Console.WriteLine (CommandLineArgumentHelper.cmd_helper);
				return;
			}
			Console.WriteLine ("Hello, AssetMerge");

			//解析命令行
			CommandLineArgumentParser arguments;
			try {
				arguments = CommandLineArgumentParser.Parse(args);
			}catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}
	
			//先搜索到文件
			FileHandle fileHandle = new FileHandle(arguments.GetMergeDirectory(), arguments.FileFilterHandle);
			List<string> files = fileHandle.GetSearchDirectoryResult();

			//合并文件
			FileMerge fileMerge = new FileMerge (arguments.GetMergeDirectory());
			fileMerge.SetVersion (1);
			fileMerge.ExecuteMerge (files, arguments.GetAssetStoragePath());

			Console.WriteLine ("FileMerge Success\n");
		}
	}
}
