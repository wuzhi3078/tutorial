﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using AssetLibrary;
namespace AssetMerge
{
	//---------------------------------------------------------
	// 文件合并
	//---------------------------------------------------------
	public class FileMerge
	{
		// 合并文件的搜索目录路径
		string mergeTargetDirectory;
		// 设置资源包版本
		int Version = 1;

		public FileMerge (string directoryName) {
			mergeTargetDirectory = System.IO.Path.GetFullPath(directoryName);
			Console.WriteLine ("资源目录: " + mergeTargetDirectory);
		}

		/// <summary>
		/// 设置版本号
		/// </summary>
		public void SetVersion (int version) {
			this.Version = version;
		}

		/// <summary>
		/// 合并资源文件 
		/// </summary>
		public void ExecuteMerge (List<string> files, string fileSave) {
			FileDeleteHandel (fileSave);

			//要合并的文件信息
			Dictionary<string, MergeFile> mdicMergeFile = new Dictionary<string, MergeFile>();

			// 文件路径规范性检查
			// 剔除大小写一样的文件路径
			for (int i = 0, L = files.Count; i < L; ++i) {
				string absolutePath = files [i];
				string relativePath = absolutePath.Replace (mergeTargetDirectory, "").PathFormat ();

				if (mdicMergeFile.ContainsKey (relativePath)) {
					Console.WriteLine ("file path is already included: " + relativePath);
					continue;
				}

				MergeFile info = new MergeFile (absolutePath, relativePath);
				mdicMergeFile.Add(relativePath, info);
			}

			List <MergeFile> mMergeFile = new List<MergeFile> (mdicMergeFile.Values);
			mMergeFile.Sort (FileInfoCompareTo);

			// 写文件						
			using (FileStream fs = new FileStream (fileSave, FileMode.OpenOrCreate)) {			
				//写资源包文件头
				AssetHeaderInfo header = new AssetHeaderInfo(Version, mMergeFile.Count);
				WriteByteToStream (fs, AssetSerialize.Serialize<AssetHeaderInfo>(header));

				int infoPosition = (int)fs.Position;
				//写资源文件信息
				foreach (MergeFile e in mMergeFile) {
					WriteByteToStream (fs, AssetSerialize.Serialize <AssetLibrary.FileInfo> (e.FileInfo));
				}

				//写资源文件的文件名
				int filenameOffset = 0;
				foreach (MergeFile e in mMergeFile) {					
					byte[] bs = e.GetFileNameByte();
					e.FileInfo.SetFileNameOffset (filenameOffset);
					filenameOffset += bs.Length;
					WriteByteToStream (fs, bs);
				}

				//写资源文件内容
				int contentOffset = 0;
				foreach (MergeFile e in mMergeFile) {
					byte[] bs = e.GetFileContent();
					e.FileInfo.SetContentOffset (contentOffset);
					contentOffset += bs.Length;
					WriteByteToStream (fs, bs);
				}

				fs.Seek (infoPosition, SeekOrigin.Begin);
				foreach (MergeFile e in mMergeFile) {
					WriteByteToStream (fs, AssetSerialize.Serialize <AssetLibrary.FileInfo> (e.FileInfo));
				}
			}			

			Console.WriteLine ("merge files:" + mMergeFile.Count);
			OutFileList (files, fileSave+".txt");
		}

		/// 向文件流写一个bute[]
		void WriteByteToStream (FileStream fs, byte[] bs) {
			fs.Write (bs, 0, bs.Length);
		}

		/// <summary>
		/// 重置输出文件
		/// </summary>
		void FileDeleteHandel (string fileSave) {
			try {
				if (File.Exists (fileSave)) {
					File.Delete (fileSave);
				}
			}catch(Exception e) {
				Console.WriteLine (string.Format ("ExecuteMerge Exception: {0}", e));
			}
		}

		/// <summary>
		/// 输出合并的文件列表
		/// </summary>
		void OutFileList (List<string> filepaths, string fileSave) {
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			foreach (string e in filepaths) {
				sb.AppendLine (e.Replace (mergeTargetDirectory, ""));
			}
			File.WriteAllText (fileSave, sb.ToString());
		}

		static int FileInfoCompareTo (MergeFile ptr1, MergeFile ptr2) {
			return string.Compare(ptr1.GetFileName () , ptr2.GetFileName ());
		}
	}
}

