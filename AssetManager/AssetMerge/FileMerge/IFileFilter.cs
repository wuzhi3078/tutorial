﻿using System;

namespace AssetMerge
{
	//---------------------------------------------------------
	// 文件过滤的接口
	//---------------------------------------------------------
	public interface IFileFilter {

		/// <summary>
		/// 文件是否符合
		/// </summary>
		bool IsCheckThrough (System.IO.FileInfo filePath);
	}
}

