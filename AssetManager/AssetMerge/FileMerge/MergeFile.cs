﻿using System;
using System.IO;
using AssetLibrary;
using FileInfo = AssetLibrary.FileInfo;
namespace AssetMerge
{
	//---------------------------------------------------------
	// 需要被合并的文件信息
	//---------------------------------------------------------
	public class MergeFile {
		
		//文件信息
		FileInfo fileinfo;

		//文件名的字节流
		byte[] filenameByte;

		string absolutePath;
		string relativePath;

		public string GetFileName () {
			return relativePath;
		}

		public byte[] GetFileNameByte () {
			return Util.ByteAlignment.AlignmentByteArray(filenameByte);
		}

		public byte[] GetFileContent() {			
			using (FileStream fs = new FileStream (this.absolutePath, FileMode.Open)) {	
				byte[] bs = new byte[fs.Length];
				int read = fs.Read (bs, 0, bs.Length);
				if (read != bs.Length)
					throw new Exception (string.Format("GetFileContent Error: {0}", relativePath));

				this.fileinfo.SetContentSize (bs.Length);
				return Util.ByteAlignment.AlignmentByteArray (bs, 4096);
			}
		}

		public MergeFile (string absolutePath, string relativePath) {
			this.fileinfo = new AssetLibrary.FileInfo();

			this.absolutePath = absolutePath; 
			this.relativePath = relativePath;
			filenameByte = System.Text.Encoding.UTF8.GetBytes (this.relativePath);
			this.fileinfo.SetFileNameLength(filenameByte.Length);
		}

		public FileInfo FileInfo {
			get {
				return fileinfo;
			}
		}
	} 
}

