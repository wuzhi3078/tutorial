﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace AssetMerge
{
	public class FileHandle
	{
		IFileFilter mfilter;
		string directionName;

		public FileHandle (string path, IFileFilter filter) {			
			this.directionName = path;
			this.mfilter = filter;
		}

		/// <summary>
		/// 根据“文件过滤”的规则获取文件列表
		/// </summary>
		public List<string> GetSearchDirectoryResult () {
			try {
				List <string> nodes = new List<string> ();
				FileHandle.OnSearchDirectory (this.directionName, (e) => {
					if (!mfilter.IsCheckThrough (e))
						return;
					nodes.Add (e.FullName);
				});
				return nodes;

			}catch(Exception e) {
				Console.WriteLine (string.Format ("ReadDirectoryContentIntoAssetInfo Exception: {0}", e));
			}
			return null;
		}

		/// <summary>
		/// 递归搜索一个目录
		/// </summary>
		/// <param name="path">Path.</param>
		/// <param name="doing">Doing.</param>
		static void OnSearchDirectory (string path, System.Action <FileInfo> doing)
		{
			DirectoryInfo theFolder = new DirectoryInfo(path);
			//遍历文件
			foreach (FileInfo NextFile in theFolder.GetFiles()) {  
				doing(NextFile);
			}
			//遍历文件夹
			foreach (DirectoryInfo NextFolder in theFolder.GetDirectories()) { 
				OnSearchDirectory(NextFolder.FullName, doing);
			}
		}
	}
}

