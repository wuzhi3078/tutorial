﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AssetMerge
{
	//-------------------------------------------------------------------------
	// 命令行帮助信息
	//-------------------------------------------------------------------------
	internal class CommandLineArgumentHelper  {
		public static string cmd_helper = "AssetMerge工具使用：\n" +
			"-s src \n\t合并的目录路径\n" +
			"-d dst \n\t合并保存的文件路径（不设定默认是asset.dat）\n"+
			"-t \"*.png|*.ogg\" \n\t需要的打包的文件类型\n";
	}

	//---------------------------------------------------------
	// 合并程序命令行过滤
	//---------------------------------------------------------

	internal class CommandLineArgumentParser {
		const string FilterAddrSource = "-s";
		const string FilterAddrTarget = "-d";
		const string FilterType = "-t";

		public static CommandLineArgumentParser Parse (string[] args) {
			return new CommandLineArgumentParser (args);
		}

		Dictionary <string, string> mcmdArgs = new Dictionary<string, string>();

		protected CommandLineArgumentParser (string[] args) {
			System.Text.StringBuilder bu = new System.Text.StringBuilder ();
			foreach (string e in args) 
				bu.Append (e + " ");
			bu.AppendLine ();
			Console.WriteLine (bu.ToString());

			mcmdArgs.Add (FilterAddrSource, string.Empty);
			mcmdArgs.Add (FilterAddrTarget, "asset.dat");
			mcmdArgs.Add (FilterType, string.Empty);

			int i = 0;
			int L = args.Length;
			for (i = 0; i < L; i += 2) {
				string e1 = args [i];
				e1 = e1.Replace ("\n", "");
				e1 = e1.Replace ("\r", "");

				if (!mcmdArgs.ContainsKey (e1)) {
					throw new Exception (e1 + "命令不对");
				}
				if (i + 1 >= L) {
					throw new Exception (e1 + "没有参数");
				}

				string e2 = args [i + 1];
				e2 = e2.Replace ("\n", "");
				e2 = e2.Replace ("\r", "");					
				mcmdArgs [e1] = e2;
			}

			if (i != args.Length)
				throw new Exception ("有无效参数");
		
			mFileFilter = new FileFilter (this);
		}

		public string this [string index] {
			get {
				if (mcmdArgs.ContainsKey (index))
					return mcmdArgs [index];
				return null;
			}
		} 

		public string GetMergeDirectory () {
			return mcmdArgs [FilterAddrSource];
		}

		/// <summary>
		/// 获取保存asset的路径
		/// </summary>
		/// <returns>保存asset的路径</returns>
		public string GetAssetStoragePath () {
			return mcmdArgs [FilterAddrTarget];
		}

		/// <summary>
		/// 文件过滤
		/// </summary>
		FileFilter mFileFilter;
		public IFileFilter FileFilterHandle {
			get {
				return mFileFilter;
			}
		}

		//---------------------------------------------------------
		// 文件过滤具体操作
		// 只验证-d -s -t -m -mf等命令
		//---------------------------------------------------------
		public class FileFilter : IFileFilter {
			Dictionary<string, bool> fileExtensionNameKeys;
			CommandLineArgumentParser cmd;

			public FileFilter (CommandLineArgumentParser cmd) {
				this.cmd = cmd;
				FormatFilterAddr ();
				FormatSaveAddr ();
				FormatFilterType ();
			}

			public bool IsCheckThrough (System.IO.FileInfo file) {
				if (fileExtensionNameKeys != null && 
					!fileExtensionNameKeys.ContainsKey (file.Extension))
					return false;

				return true;
			}

			bool IsCommandExistKey (string key) {
				string e = cmd [key];

				if (string.IsNullOrEmpty (e))
					return false;
				
				return true;
			} 

			void FormatFilterAddr () {
				if (!IsCommandExistKey (CommandLineArgumentParser.FilterAddrSource))
					throw new Exception ("-s 输入合并的目录路径");

				string e = cmd [CommandLineArgumentParser.FilterAddrSource];
				if (!System.IO.Directory.Exists (e)) {
					throw new Exception ("-s 目录不存在： " + e);	
				}
			}

			void FormatSaveAddr () {
				if (!IsCommandExistKey (CommandLineArgumentParser.FilterAddrTarget)) {
					throw new Exception ("-d 保存路径必须存在");
				}					
			}

			void FormatFilterType () {
				if (!IsCommandExistKey (CommandLineArgumentParser.FilterType))
					return ;

				fileExtensionNameKeys = new Dictionary<string, bool>();
				string types = cmd [CommandLineArgumentParser.FilterType];
				string[] ss = types.Split ('|');
				for (int i = 0; i< ss.Length; ++ i) {
					string e = ss [i];
					if (string.IsNullOrEmpty (e) || e.Length == 0) throw new Exception (e + " 扩展名不对");

					if (e == "*.*" || e == "*." || e == ".*") {
						fileExtensionNameKeys = null;
						Console.WriteLine ("全部文件类型： " + e);
						break;
					}

					if (e [0] == '*') e = e.Replace ("*", "");
					if (e [0] != '.') throw new Exception (e + " 扩展名不对");
					fileExtensionNameKeys.Add(e, true);
					Console.WriteLine ("打包的文件类型： " + e);
				}

				return;
			}
		}
	}
}

