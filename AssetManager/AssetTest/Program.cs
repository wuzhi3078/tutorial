﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using AssetLibrary;
namespace AssetTest
{
	//-------------------------------------------------------------------------
	// 测试工程入口
	//-------------------------------------------------------------------------
	public class MainClass {
		public static void Main(string[] args) {
			if (args.Length == 1 && args [0] == "--help") {
				Console.WriteLine (Config.cmd_helper);
				return;
			}			

			Console.WriteLine("Hello, AssetTest!");

			CommandArgs cmd = null;
			try {
				cmd = CommandArgs.Parse (args);
			}catch (Exception e) {
				Console.WriteLine (e.ToString());
				return;
			}

			new Simulation ().GameRun (cmd);
		}
	}

	//-------------------------------------------------------------------------
	// 模拟测试类
	//-------------------------------------------------------------------------
	internal class Simulation {
		// 游戏输入
		CommandArgs cmd;
		// 游戏模拟
		GameEngine mEngine;
		// 记录时间
		Stopwatch watch; 

		public void GameRun(CommandArgs cmdargs) {
			this.cmd = cmdargs;

			GameEngine engine = new GameEngine ();
			this.mEngine = engine;

			foreach (string e in cmd.GetInputInfo().GetIndexFiles()) {
				engine.AddTestFileList (e);
			}

			// 随机访问并且统计时间
			watch = new Stopwatch ();
			watch.Start ();
			engine.SetGameAssetManager (OnPreLoadAssetBundle ());
			engine.Run (cmd.GetInputInfo().MaxThread);
			WaitForComplete ();
			watch.Stop ();
			
			// 完成时输出信息
			OnComplete ();
		}

		/// <summary>
		/// 提前预载资源包
		/// </summary>
		IAssetBundle OnPreLoadAssetBundle () {
			bool isAssetMode = cmd.GetInputInfo ().IsAssetBundle;

			string paths = isAssetMode ? cmd.GetInputInfo ().GetResourceFiles ()[0]:
				cmd.GetInputInfo().GetResourceDirectorys() [0];
			IAssetBundle assetBundle = Assetfactory.CreateAssetBundle(paths, isAssetMode);
			return assetBundle;
		}

		/// <summary>
		/// 等待模拟线程完成
		/// </summary>
		void WaitForComplete () {
			while (!mEngine.IsGameComplete ()) {				
			}
		}

		/// <summary>
		/// 模拟结束时的操作
		/// </summary>
		void OnComplete () {
			Console.WriteLine (string.Format ("cost: {0}.{1:D3}", watch.ElapsedMilliseconds / 1000 , watch.ElapsedMilliseconds%1000));

			List <GameScene> scenes = mEngine.Scenes;
			int sum = 0;
			int size = 0;
			for (int i = 0, L = scenes.Count; i < L; ++i) {
				scenes [i].PrintInfo ();
				sum += scenes [i].HandleNumber;
				size += scenes [i].ReadCapacitySize;
			}				

			Console.WriteLine (string.Format ("总读取文件次数：{0} 读取容量大小：{1}m{2}byte = {3}byte \n每秒读取: {4} m/s", 
				sum, size/(1024*1024), size%(1024*1024), size, 
				size * 1.0f/(1024*1024) / (watch.ElapsedMilliseconds*1.0f / 1000)));
		}
	}
}
