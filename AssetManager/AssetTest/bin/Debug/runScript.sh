#!/bin/sh

monoEnvironment="/Applications/Unity/MonoDevelop.app/Contents/Frameworks/Mono.framework/Versions/Current/bin/mono"

currentPath="/Users/apple/Documents/Workspaces/tutorial/AssetManager/AssetTest/bin/Debug/"
indexdir="/Users/apple/Documents/Workspaces/tutorial/AssetManager/AssetMerge/bin/Debug/"

exeFile=${currentPath}"AssetTest.exe"

indexfile1=${indexdir}"asset_HJD666666666.dat.txt"
assetfile1=${indexdir}"asset_HJD666666666.dat"
direction1="/Users/apple/Documents/Workspaces/wuzhi/HJD666666666"

indexfile2=${indexdir}"asset_dczg.dat.txt"
assetfile2=${indexdir}"asset_dczg.dat"
direction2="/Users/apple/Documents/Workspaces/wuzhi/dczg"

maxthread="5"

cd $currentPath;
$monoEnvironment $exeFile -t "res" -readIndex $indexfile1 -loadRes $assetfile1
$monoEnvironment $exeFile -t "dir" -readIndex $indexfile1 -loadDir $direction1
$monoEnvironment $exeFile -t "res" -readIndex $indexfile2 -loadRes $assetfile2
$monoEnvironment $exeFile -t "dir" -readIndex $indexfile2 -loadDir $direction2