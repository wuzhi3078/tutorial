﻿using System;
using AssetLibrary;
namespace AssetTest
{
	public class Assetfactory {
		public static IAssetBundle CreateAssetBundle (string assetPath, bool isAssetBundle) {
			if (isAssetBundle)
				return new AssetBundle (assetPath);
			else
				return new SystemResource (assetPath);
		}
	}
}

