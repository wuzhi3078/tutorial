﻿using System;
using AssetLibrary;
namespace AssetTest
{
	//-------------------------------------------------------------------------
	// 系统直接读取文件
	// 可添加多个目录搜索路径
	//-------------------------------------------------------------------------
	public class SystemResource : IAssetBundle {
		string currentSystemPath;

		public SystemResource (string path) {
			currentSystemPath = path;
		}

		public bool IsExist (string filePath) {
			string absolutePath = System.IO.Path.Combine(currentSystemPath, filePath);
			return System.IO.File.Exists (absolutePath);
		} 

		public byte[] Load (string filePath) {
			string absolutePath = System.IO.Path.Combine(currentSystemPath, filePath);
			return System.IO.File.ReadAllBytes (absolutePath);
		}

		public void UnLoad (string path) {

		}
	}
}

