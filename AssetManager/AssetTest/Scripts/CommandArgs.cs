﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace AssetTest
{
	internal class CommandArgs
	{
		public static CommandArgs Parse (string[] args) {
			return new CommandArgs (args);
		}

		const string cmd_type = "-t";
		const string cmd_typeRes = "res";
		const string cmd_typeDir = "dir";

		const string cmd_readIndexFile = "-readIndex";
		const string cmd_loadResource = "-loadRes";
		const string cmd_loadDir = "-loadDir";

		const string cmd_thread = "-thread";//测试的线程数
		const string cmd_count = "-count";//测试读取的次数

		Dictionary <string, List<string>> mdicCommand;

		protected CommandArgs (string[] args)
		{
			System.Text.StringBuilder argsBuild = new System.Text.StringBuilder ();
			foreach (string e in args) {
				argsBuild.Append (e + " ");
			}
			argsBuild.AppendLine ();
			Console.WriteLine (argsBuild.ToString());

			mdicCommand = new Dictionary<string, List<string>> ();
			mdicCommand.Add (cmd_readIndexFile, new List<string> ());
			mdicCommand.Add (cmd_loadResource, new List<string>());
			mdicCommand.Add (cmd_loadDir, new List<string>());
			mdicCommand.Add (cmd_type,new List<string>());
			mdicCommand.Add (cmd_thread, new List<string> ());
			mdicCommand.Add (cmd_count, new List<string> ());

			for (int i = 0; i < args.Length; i += 2) {
				string e1 = args [i];

				e1 = e1.Replace ("\n", "");
				e1 = e1.Replace ("\r", "");

				if (!mdicCommand.ContainsKey (e1))
					throw new Exception ("命令错误：" + e1);

				if (i + 1 >= args.Length)
					throw new Exception ("没有参数：" + e1);

				string e2 = args [i + 1];
				e2 = e2.Replace ("\n", "");
				e2 = e2.Replace ("\r", "");

				mdicCommand [e1].Add (e2);
			}

			m_InputInfo = new InputInfo (this);
		}

		public List<string> this [string e] {
			get {
				if (mdicCommand.ContainsKey (e))
					return mdicCommand [e];
				return null;
			}
		}

		InputInfo m_InputInfo;
		public InputInfo GetInputInfo () {
			return m_InputInfo;
		}

		public class InputInfo {
			bool misAssetOrSysApi = true;
			CommandArgs parser;
			int maxthread;
			int maxtestCount;

			public InputInfo (CommandArgs parser) {
				this.parser = parser;

				MakeApiType ();
				MakeSureIndexFile ();
				MakeSurethreadAndCount();
			} 

			void MakeApiType () {
				List<string> types = parser [CommandArgs.cmd_type];
				if (types == null || types.Count == 0) {
					misAssetOrSysApi = true;
					Console.WriteLine ("资源读取类型没有输入，默认是资源包读取方式");
					return;
				}

				string e = types [0];
				if (e == CommandArgs.cmd_typeDir)
					misAssetOrSysApi = false;
			}

			void MakeSureIndexFile () {
				List<string> files = parser [CommandArgs.cmd_readIndexFile];
				if (files == null || files.Count == 0)
					throw new Exception (string.Format("索引文件不能为空，请输入 {0} file", 
						CommandArgs.cmd_readIndexFile));

				foreach (string file in files) {
					if (!System.IO.File.Exists (file))
						throw new Exception (string.Format("请确认文件 {0} 存在", file));
				}
			}

			void MakeSureResourceFiles () {
				if (!misAssetOrSysApi)
					return;
				
				List<string> assets = this.GetResourceFiles ();				
				foreach (string file in assets) {
					if (!File.Exists (file)) {
						throw new Exception (string.Format("请确认文件 {0} 存在", file));
					}
				}
			}

			void MakeSureDirections () {
				if (misAssetOrSysApi)
					return;

				List<string> dirs = this.GetResourceDirectorys ();
				foreach (string dir in dirs)
					if (!System.IO.Directory.Exists (dir))
						throw new Exception (string.Format("请确认目录 {0} 存在", dir));
			}

			void MakeSurethreadAndCount () {
				List <string> threadNumber = parser [CommandArgs.cmd_thread];
				if (threadNumber == null || threadNumber.Count == 0) {
					this.maxthread = 5;
				} else {
					if (!int.TryParse (threadNumber [0], out this.maxthread)) {
						this.maxthread = 5;
					}
				}

				List <string> testReadNumber = parser [CommandArgs.cmd_count];
				if (testReadNumber == null || testReadNumber.Count == 0) {
					this.maxtestCount = 1000;
				} else {
					if (!int.TryParse (testReadNumber [0], out this.maxtestCount)) {
						this.maxtestCount = 1000;
					}
				}
			}

			public List<string> GetIndexFiles () {
				return parser [CommandArgs.cmd_readIndexFile];
			}

			public List<string> GetResourceFiles () {
				return parser [CommandArgs.cmd_loadResource];
			}

			public List <string> GetResourceDirectorys () {
				return parser [CommandArgs.cmd_loadDir];
			}

			public bool IsAssetBundle {
				get { 
					return misAssetOrSysApi;
				}
			}

			public int MaxThread {
				get { return this.maxthread;}
			}

			public int MaxTestTime {
				get { return this.maxtestCount;}
			}
		}
	}
}

