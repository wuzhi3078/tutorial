﻿using System;
using System.Threading;
namespace AssetTest
{
	//-------------------------------------------------------------------------
	// 游戏读取资源的情景，GameScene是单独存在且同时都在更新
	//-------------------------------------------------------------------------
	public class GameScene {
		GameEngine mEngine;
		int mSceneId;

		// 总操作的次数
		int sumCount = 0;
		// 文件不存在的次数
		int failedError = 0;
		// 读取文件字节失败次数
		int failedRead = 0;
		// 读取的文件字节为0的次数
		int zeroRead = 0;
		// 读取的文件byte容量大小
		int readCapacitySize = 0;
		// update是否已经完成
		bool misUpdateComplete = false;

		public GameScene (GameEngine engine, int id) {
			mEngine = engine;
			mSceneId = id;
			misUpdateComplete = false;

			try {
				new Thread(Update).Start();
			} catch (Exception e) {					
				Console.WriteLine(e.ToString());
			}
		}

		public void Update () {
			while (true) {
				if (mEngine.IsCountComplete ())
					break;

				string path = mEngine.GetRandomGameResFilePath ();
				if (string.IsNullOrEmpty (path)) {					
					continue;
				}

				sumCount++;
				if (!mEngine.AssetManger.IsExist (path)) {
					failedError++;
					continue;
				}
				byte[] bs = mEngine.AssetManger.Load (path);
				int bsLength = bs == null ? 0 : bs.Length;
				if (bs == null)	failedRead++;
				if (bsLength == 0)	zeroRead++;
				readCapacitySize += bsLength;

				//Console.WriteLine (BitConverter.ToString (bs));
			}

			this.misUpdateComplete = true;
		}

		/// <summary>
		/// 打印操作结果的信息
		/// </summary>
		public void PrintInfo () {
			Console.WriteLine (string.Format("scene: {0} sum: {1} not: {2} failed: {3} zero:{4} readSize: {5}m{6}byte",
				mSceneId, sumCount, failedError, failedRead, zeroRead, readCapacitySize / (1024*1024), readCapacitySize%(1024*1024)));
		}

		/// <summary>
		/// 总操作次数
		/// </summary>
		public int HandleNumber {
			get { 
				return sumCount;
			}
		}

		/// <summary>
		/// 获取读到的文件内容大小
		/// </summary>
		public int ReadCapacitySize {
			get {
				return readCapacitySize;
			}
		}

		/// <summary>
		/// 是否线程更新已经完成
		/// </summary>
		public bool IsUpdateComplete {
			get { 
				return misUpdateComplete;
			}
		}
	}
}

