﻿using System;

namespace AssetTest
{
	internal static class Config {

		public static string cmd_helper = "测试工程的命令行参数：\n" +
		                                  "-t res|dir \n\tres用的是资源方式测试， dir用的是系统目录方式测试\n" +
		                                  "-readIndex filePath \n\t读取文件名索引文件\n" +
		                                  "-loadRes assetPath \n\t如果-t用的是res则有效，asset资源包的路径\n" +
		                                  "-loadDir dirPath \n\t如果-t用的是dir则有效，dirPath系统目录路径n" +
		                                  "-thread n \n\t测试读取的线程数，不设定默认是5\n";
	}
}

