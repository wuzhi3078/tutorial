﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using AssetLibrary;

namespace AssetTest
{
	//-------------------------------------------------------------------------
	// 模拟游戏读取的情景
	//-------------------------------------------------------------------------
	public class GameEngine
	{
		// 资源路径
		string [] gameTestFilePath;
		// 最大的读取次数
		int maxTestCount;
		// 资源管理
		IAssetBundle assetManager;
		// 读取线程
		List <GameScene> mGameRunScene;

		public GameEngine () {
			gameTestFilePath = new string[0];
		}

		public void Run (int maxScene) {
			this.maxTestCount = gameTestFilePath.Length;
			this.mGameRunScene = new List<GameScene> ();
			for (int i = 0; i < maxScene; ++i) {
				mGameRunScene.Add(new GameScene (this, i));
			}
		}

		public void AddTestFileList (string file) {
			string[] res = File.ReadAllLines (file);
			int pre = gameTestFilePath.Length;
			Array.Resize(ref gameTestFilePath, pre + res.Length);
			Array.ConstrainedCopy(res, 0, gameTestFilePath, pre, res.Length);
		}

		public void SetGameAssetManager (IAssetBundle asset) {
			assetManager = asset;
		}

		// 安全读取
		static Object mLockTestCount = new Object ();
		private int GetFileIndexSafe () {
			lock (mLockTestCount) {
				if (maxTestCount > 0) {
					maxTestCount--;
					return maxTestCount;
				}
				return -1;
			}
		}

		/// <summary>
		/// 获取文件路径
		/// </summary>
		/// <returns>文件路径</returns>
		public string GetRandomGameResFilePath () {
			int index = GetFileIndexSafe ();
			if (index > -1)
				return gameTestFilePath [index];
			return null;
		}

		// 是否读取次数已经完成
		public bool IsCountComplete () {
			return this.maxTestCount <= 0;
		}

		// 资源管理
		public IAssetBundle AssetManger {
			get { 
				return assetManager;
			}
		}

		// 游戏引擎模拟更新完成
		public bool IsGameComplete () {
			for (int i = 0, L = mGameRunScene.Count; i < L; ++i)
				if (!mGameRunScene [i].IsUpdateComplete)
					return false;

			return true;
		}

		// 游戏模拟的场景
		public List <GameScene> Scenes {
			get { 
				return this.mGameRunScene;
			}
		}
	}
}

