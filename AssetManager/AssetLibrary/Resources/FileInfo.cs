﻿using System;
using System.Runtime.InteropServices;

namespace AssetLibrary
{
	//---------------------------------------------------------
	//文件索引节点
	//---------------------------------------------------------
	[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi, Pack = Util.PragmaPack)]
	public class FileInfo : IAssetSerialize
	{		
		int  contentOffset; // 文件内容在资源包的偏移
		int  contentSize;	 // 文件内容的大小
		int  filenameOffset; // 文件名的偏移
		int  filenameLength; // 文件名的长度

		public FileInfo () {			
		}

		// 文件名在资源包的偏移
		public int FileNameOffset {
			get {return filenameOffset;}
		}

		// 文件名在资源包的长度
		public int FileNameLength {
			get { return filenameLength; }
		}			

		// 真实内容在资源包的偏移
		public int Offset {
			get { return contentOffset;}
		}

		// 真实内容的大小
		public int Size {
			get {return contentSize;}
		}

		public void SetFileNameOffset (int mfilenameoffset) {filenameOffset = mfilenameoffset;}
		public void SetFileNameLength (int l) {filenameLength = l; }
		public void SetContentOffset (int fet) {contentOffset  = fet;}
		public void SetContentSize (int si) {contentSize = si;}

		/// <summary>
		/// 大小端互转
		/// </summary>
		public object OnBeforeSerialize (bool IsLittleEndian) {			
			if (IsLittleEndian) {
				FileInfo e = (FileInfo) this.MemberwiseClone();
				e.OnReversal ();
				return e;
			}
			return this;
		}

		public object OnAfterDeserialize (bool IsLittleEndian) {
			if (IsLittleEndian) {
				this.OnReversal ();
			}
			return this;
		}

		public void OnReversal () {
			contentOffset = Endian.SwapInt32 (this.contentOffset);
			contentSize = Endian.SwapInt32 (this.contentSize);
			filenameLength = Endian.SwapInt32 (this.filenameLength);
		}
	}
}

