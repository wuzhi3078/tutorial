﻿using System;

namespace AssetLibrary
{
	//---------------------------------------------------------
	// 序列化前需要经过大小端转换操作
	//---------------------------------------------------------
	public interface IAssetSerialize {

		/// <summary>
		/// 序列化
		/// </summary>
		/// <param name="isLittleEndian">true CPU小端 false CPU大端</param>
		object OnBeforeSerialize (bool isLittleEndian);

		/// <summary>
		/// 反转int等
		/// </summary>
		void OnReversal ();

		/// <summary>
		/// 反序列化
		/// </summary>
		/// <param name="isLittleEndian">true CPU小端 false CPU大端</param>
		object OnAfterDeserialize (bool isLittleEndian);
	}
}

