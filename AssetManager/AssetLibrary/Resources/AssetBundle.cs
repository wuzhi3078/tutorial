﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace AssetLibrary
{
	//---------------------------------------------------------
	// 资源包
	//---------------------------------------------------------
	public class AssetBundle: IAssetBundle
	{		
		public static byte[] AssetTypeFlag = new byte[4] {(byte)'w', (byte)'d', (byte)'s', (byte)'j'};

		//文件信息
		Dictionary <string, IAssetFile> mdicFiles; 

		//头部信息
		AssetHeaderInfo headerInfo; 

		//资源文件流
		FileStream mAssetfileStream; 

		//是否是有效的资源包
		bool misValid = false; 

		//资源包路径
		string assetPath; 

		public bool IsExist (string filepath) {
			return misValid && mdicFiles.ContainsKey (filepath.PathFormat());
		}

		public byte[] Load (string filepath) {
			return mdicFiles [filepath.PathFormat ()].GetFileContent ();
		}

		public AssetBundle (string assetPackagePath) {
			this.assetPath = assetPackagePath;

			if (!File.Exists (assetPackagePath)) {
				misValid = false;
				return;
			}

			try{
				mAssetfileStream = new FileStream (this.assetPath, FileMode.Open);

				// 读取资源包头
				headerInfo = AssetSerialize.Deserialize<AssetHeaderInfo> (mAssetfileStream);
				if (!IsAssetBundleType (headerInfo.Type)) {
					CloseFileStream();
					misValid = false;
					return;
				}

				if (IsLog) Console.WriteLine ("sum:"+headerInfo.FileLength);

				// 读出资源文件信息
				List<AssetFile> mAssetFiles = new List<AssetFile> ();
				for (int i = 0; i < headerInfo.FileLength; ++i) {
					FileInfo e = AssetSerialize.Deserialize <FileInfo> (mAssetfileStream);
					AssetFile p = new AssetFile (e, this);
					mAssetFiles.Add (p);
				}

				// 读出资源文件名称
				for (int i=0; i < headerInfo.FileLength; ++i) {
					AssetFile p = mAssetFiles [i];
					byte[] bs = new byte[Util.ByteAlignment.AlignLength(p.GetFileInfo().FileNameLength)]; 
					int read = mAssetfileStream.Read (bs, 0, bs.Length);
					if (read != bs.Length)
						throw new Exception ("read filenam error");
					p.SetFileName (System.Text.Encoding.UTF8.GetString (bs).Replace("\0",""));
					if (IsLog) Console.WriteLine (p.GetFileName());
				}

				// 重置资源文件的偏移 
				int contentOffset = (int)mAssetfileStream.Position;
				mdicFiles = new Dictionary<string, IAssetFile> ();
				foreach (IAssetFile e in mAssetFiles) {
					AssetLibrary.FileInfo fileinfo = e.GetFileInfo ();
					fileinfo.SetContentOffset (fileinfo.Offset + contentOffset);
					mdicFiles.Add (e.GetFileName (), e);
				}

				//是一个有效的资源包
				misValid = true;

			}catch(Exception e) {
				CloseFileStream ();
				misValid = false;
			}
		}

		public virtual void Destroy () {
			this.CloseFileStream ();
		}


		void CloseFileStream () {
			if (this.mAssetfileStream != null) {
				this.mAssetfileStream.Close ();
				this.mAssetfileStream.Dispose();
				this.mAssetfileStream = null;
			}
		}

		/// <summary>
		/// 资源头部的信息
		/// </summary>
		public AssetHeaderInfo HeaderInfo {
			get {return headerInfo;}
		}

		/// <summary>
		/// 从资源文件包获取字节，从offset开始，获取size个
		/// </summary>
		public virtual byte[] GetBytesFromOffsetAndSize (int offset, int size) {
			lock (mLockRead) {
				try{
					mAssetfileStream.Seek (offset, SeekOrigin.Begin);
					byte[] bs = new byte[size];
					int read = mAssetfileStream.Read (bs, 0, size);
					if (read != size)
						return null;

					return bs;
				}catch(Exception e){
					return null;
				}
			}
		}

		object mLockRead = new object (); 

		/// <summary>
		/// 判断文件是否为资源文件类型
		/// </summary>
		static bool IsAssetBundleType (byte[] bs) {
			if (bs == null || bs.Length != AssetTypeFlag.Length)
				return false;
			
			for (int i = 0; i< AssetTypeFlag.Length; ++i)
				if (AssetTypeFlag [i] != bs [i])
					return false;
			
			return true;
		}

		/// <summary>
		/// 是否是有效的资源包(判断资源包的类型)
		/// </summary>
		public bool IsValid () {
			return misValid;
		}

		static bool IsLog = false;
	}
}

