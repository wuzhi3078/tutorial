﻿using System;

namespace AssetLibrary
{
	//---------------------------------------------------------
	// 资源内部文件信息
	//---------------------------------------------------------
	public class AssetFile : IAssetFile {

		// 文件信息头
		FileInfo mfileInfo;

		// 所在的资源包
		AssetBundle asset;

		// 文件名
		string fileName;

		public AssetFile () {}

		public AssetFile (FileInfo p, AssetBundle asset) {
			this.mfileInfo = p;
			this.asset = asset;
		}

		public virtual string GetFileName () {
			return fileName;
		}

		public virtual FileInfo GetFileInfo () {
			return this.mfileInfo;
		}

		public virtual byte[] GetFileContent () {
			return asset.GetBytesFromOffsetAndSize (this.mfileInfo.Offset,  this.mfileInfo.Size);
		}

		public virtual void SetFileName (string fileName) {
			this.fileName = fileName;
		}

		public virtual void SetFileContent (byte[] bs) {
			
		}

		public virtual void SetFileInfo (FileInfo info) {
			this.mfileInfo = info;
		}
	}
}

