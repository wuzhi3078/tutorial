﻿using System;
using System.Runtime.InteropServices;

namespace AssetLibrary
{
	//---------------------------------------------------------
	// 字节转换
	//---------------------------------------------------------
	public class AssetSerialize {
		/// <summary>
		/// 顺序结构到字节的内存布局
		/// </summary>
		public static byte[] ObjectToByte<T> (T structObj) {
			int size = Marshal.SizeOf(typeof (T));
			IntPtr buffer = Marshal.AllocHGlobal(size);
			try
			{
				Marshal.StructureToPtr(structObj, buffer, false);
				byte[] bytes = new byte[size];
				Marshal.Copy(buffer, bytes, 0, size);
				return bytes;
			}
			finally
			{
				Marshal.FreeHGlobal(buffer);
			}
		}

		/// <summary>
		/// 字节排序序列到顺序结构
		/// </summary>
		public static T ByteToObject <T> (byte[] bytes) {
			int size = Marshal.SizeOf(typeof (T));
			IntPtr buffer = Marshal.AllocHGlobal(size);
			try
			{
				Marshal.Copy(bytes, 0, buffer, size);
				return(T) Marshal.PtrToStructure(buffer, typeof (T));
			}
			finally
			{
				Marshal.FreeHGlobal(buffer);
			}
		}

		/// <summary>
		/// 对象转换成字节数组
		/// </summary>
		public static byte[] Serialize <T>(T structObj) where T: IAssetSerialize
		{
			structObj = (T) structObj.OnBeforeSerialize(Util.IsLittleEndian);
			return ObjectToByte <T> (structObj);
		}

		/// <summary>
		/// 字节数组转换成对象
		/// </summary>
		public static T Deserialize <T>(byte[] bytes) where T:IAssetSerialize
		{
			return (T) ByteToObject <T> (bytes).OnAfterDeserialize (Util.IsLittleEndian);
		}

		public static T Deserialize <T>(System.IO.FileStream fs) where T:IAssetSerialize {
			int size = Marshal.SizeOf (typeof (T));
			byte[] bs = new byte[size];
			fs.Read (bs, 0, size);
			return Deserialize <T> (bs);
		}

		/// <summary>
		/// 获取类型T的大小
		/// </summary>
		public static int GetSizeOf <T> ()  where T:IAssetSerialize {
			return Marshal.SizeOf (typeof (T));
		}
	}
}

