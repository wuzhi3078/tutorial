﻿using System;
using System.Runtime.InteropServices;

namespace AssetLibrary
{
	//---------------------------------------------------------
	// 资源头部信息
	//---------------------------------------------------------
	[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi, Pack = Util.PragmaPack)]
	public class AssetHeaderInfo : IAssetSerialize {

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
		byte[] assetType;	// 资源包的类型
		int assetVersion;	// 资源包的版本号
		int fileInfoCount;	// 资源包包括的文件数量

		public AssetHeaderInfo () {
			MarkFileType ();
		}

		public AssetHeaderInfo (int version, int fileCount) : base() {
			this.assetVersion = version;
			this.fileInfoCount = fileCount;
		}

		public int Version {get {return this.assetVersion;}}

		public byte[] Type {get {return this.assetType;}}

		public int FileLength {get {return this.fileInfoCount;}}

		public object OnBeforeSerialize (bool IsLittleEndian) {			
			if (IsLittleEndian) {	
				AssetHeaderInfo e = (AssetHeaderInfo) this.MemberwiseClone();
				e.MarkFileType ();
				e.OnReversal ();
				return e;
			}
			return this;
		}

		public object OnAfterDeserialize (bool IsLittleEndian) {
			if (IsLittleEndian) {
				this.OnReversal ();
			}
			return this;
		}

		public void OnReversal () {
			assetVersion = Endian.SwapInt32 (this.assetVersion);
			fileInfoCount = Endian.SwapInt32 (this.fileInfoCount);
		}

		void MarkFileType () {
			assetType = new byte[4];
			Array.Copy(AssetBundle.AssetTypeFlag, assetType, 4);
		}
	}
}

