﻿using System;

namespace AssetLibrary
{
	/// <summary>
	/// 文件
	/// </summary>
	public interface IAssetFile
	{
		/// <summary>
		/// 文件信息
		/// </summary>
		/// <returns>文件信息</returns>
		FileInfo GetFileInfo ();

		/// <summary>
		/// 获取文件名
		/// </summary>
		/// <returns>文件名</returns>
		string GetFileName ();

		/// <summary>
		/// 获取文件内容
		/// </summary>
		/// <returns>文件二进制内容</returns>
		byte[] GetFileContent ();

		/// <summary>
		/// 设置文件名
		/// </summary>
		/// <param name="fileName">文件名</param>
		void SetFileName (string fileName);

		/// <summary>
		/// 设置文件内容
		/// </summary>
		/// <param name="bs">Bs.</param>
		void SetFileContent (byte[] bs);

		/// <summary>
		/// 文件信息
		/// </summary>
		/// <param name="info">Info.</param>
		void SetFileInfo (FileInfo info);
	}
}

