﻿using System;

namespace AssetLibrary
{
	public interface IAssetBundle {
		
		/// <summary>
		/// 文件是否存在
		/// </summary>
		/// <param name="path">文件相对路径</param>
		bool IsExist (string filepath);

		/// <summary>
		/// 读取文件的内容
		/// </summary>
		/// <param name="path">文件相对路径</param>
		byte[] Load (string filepath);
	}
}

