﻿using System;
using System.Security.Cryptography;  
using System.Text;

namespace AssetLibrary
{
	public static class Util
	{
		/// <summary>
		/// 内存对齐字节
		/// </summary>
		public const int PragmaPack = 8;

		/// <summary>
		/// CPU序列大小端判断
		/// </summary>
		public static bool IsLittleEndian {
			get {
				return BitConverter.IsLittleEndian;
			}
		}

		/// <summary>
		/// 路径格式化
		/// </summary>
		public static string PathFormat (this string path) {
			return path.Replace ("\\", "/").ToLower ();
		}

		/// <summary>
		/// SDBMs the hash.
		/// </summary>
		public static uint SDBMHash(string str) {
			uint hash = 0;
			for (int i = 0, L = str.Length; i < L; ++i)
				hash = str [i] + (hash << 6) + (hash << 16) - hash;
			return (hash & 0x7FFFFFFF);
		}

		/// <summary>
		/// 16位md5 hash
		/// </summary>
		public static string MD5Encrypt(string strText) {     
			MD5 md5 = new MD5CryptoServiceProvider();  
			byte[] result = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(strText));
			return System.Text.Encoding.UTF8.GetString(result);  
		}  

		public static class ByteAlignment {
			/// <summary>
			/// 根据n对齐字节数组
			/// </summary>
			public static byte[] AlignmentByteArray (byte[] bs, int n = Util.PragmaPack) {
				int length = AlignLength (bs.Length, n);
				if (length == bs.Length)
					return bs;

				Array.Resize (ref bs, length);
				return bs;
			} 

			/// <summary>
			/// 对齐length是n的倍数
			/// </summary>
			public static int AlignLength (int length, int n = Util.PragmaPack) {
				int remainder = length % n;
				if (remainder == 0)
					return length;
				return length + n - remainder;
			}
		}
	}
}

